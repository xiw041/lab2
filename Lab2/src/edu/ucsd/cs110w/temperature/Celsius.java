/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author lucia_77kaguya
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		return "" + getValue() + " C";
	}
	@Override
	public Temperature toCelsius() {
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		Temperature outTemp = new Fahrenheit( getValue()*1.8f + 32.0f );
		return outTemp;
	}
	@Override
	public Temperature toKelvin() {
		Temperature outTemp = new Kelvin( getValue() + 273.0f );
		return outTemp;	
	}

}
