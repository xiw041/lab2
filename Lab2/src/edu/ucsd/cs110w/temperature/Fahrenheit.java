/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author lucia_77kaguya
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		return "" + getValue() + " F";
	}
	@Override
	public Temperature toCelsius() {
		Temperature outTemp = new Celsius( (getValue()-32.0f)/1.8f ); 
		return outTemp;
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
	@Override
	public Temperature toKelvin() {
		Temperature outTemp = new Kelvin( (getValue()-32.0f)/1.8f + 273.0f );
		return outTemp;	
	}

}
